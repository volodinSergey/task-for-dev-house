import { TodoService } from "../infrastructure/services/TodoService.js"

const todoService = new TodoService()

const onClickAddTaskButton = textForAdding => todoService.addTask(textForAdding)

const onClickDeleteTaskButton = id => todoService.deleteTask(id)

const onClickEditTaskButton = (editedTaskId, textForEditing) => todoService.editTask(editedTaskId, textForEditing)

export const useTodoStore = () => ({
    onClickAddTaskButton,
    onClickDeleteTaskButton,
    onClickEditTaskButton
})