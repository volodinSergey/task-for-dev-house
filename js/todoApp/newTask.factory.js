const NO_NAME_TEXT = 'No name'

export const newTaskFactory = newTaskText => ({
    id: Math.random(),
    text: newTaskText || NO_NAME_TEXT,
})