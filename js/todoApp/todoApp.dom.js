const domState = {
    tasksParentSelector: '.tasks-list',
    taskSelector: 'task',
    taskTemplateSelector: 'task-content',
    taskTextSelector: '.task__top-text',
    taskTemplateContainer: 'task__template-container',

    todoInputSelector: '.todo__input',
    todoButtonSelector: '.todo__button',

    deleteButtonSelector: 'delete-button',
    deleteButtonIconSelector: 'delete-button__icon'

}

const {
    tasksParentSelector,
    taskSelector,
    taskTemplateSelector,
    taskTextSelector,
    taskTemplateContainer,
    todoInputSelector,
    todoButtonSelector,
    deleteButtonSelector,
    deleteButtonIconSelector

} = domState

export {
    tasksParentSelector,
    taskSelector,
    taskTemplateSelector,
    taskTextSelector,
    taskTemplateContainer,
    todoInputSelector,
    todoButtonSelector,
    deleteButtonSelector,
    deleteButtonIconSelector
}