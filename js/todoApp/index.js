import { todoAppModel } from '../infrastructure/services/TodoService.js'

import { cleanDomListBeforeRender } from '../shared/utils/cleanDomListBeforeRender.js'
import { cleanInputValue } from '../shared/utils/cleanInputValue.js'

import {
    tasksParentSelector,
    taskSelector,
    todoInputSelector,
    todoButtonSelector,
    deleteButtonSelector,
    deleteButtonIconSelector
} from "./todoApp.dom.js"

import { render, checkForNoTasks } from "./todoApp.render.js"
import { useTodoStore } from './todoApp.store.js'

export const todoApp = () => {
    const tasksParent = document.querySelector(tasksParentSelector)

    checkForNoTasks(todoAppModel, tasksParent) || render(todoAppModel, tasksParent)

    const {
        onClickAddTaskButton,
        onClickDeleteTaskButton,
        onClickEditTaskButton
    } = useTodoStore()

    const addTaskInput = document.querySelector(todoInputSelector)
    const addTaskButton = document.querySelector(todoButtonSelector)

    addTaskButton.addEventListener('click', (e) => {
        e.preventDefault();

        onClickAddTaskButton(addTaskInput.value)

        cleanDomListBeforeRender(tasksParent)

        render(todoAppModel, tasksParent)

        cleanInputValue(addTaskInput)
    })

    tasksParent.addEventListener('click', (e) => {
        if (e.target.classList.contains(deleteButtonSelector) || e.target.classList.contains(deleteButtonIconSelector)) {
            const chousenTask = e.target.closest(`.${taskSelector}`)
            const chousenTaskId = +chousenTask.dataset.id

            onClickDeleteTaskButton(chousenTaskId)

            cleanDomListBeforeRender(tasksParent)

            checkForNoTasks(todoAppModel, tasksParent) || render(todoAppModel, tasksParent)
        }
    })

    tasksParent.addEventListener('click', (e) => {
        if (!e.target.classList.contains('edit-button__icon')) return

        const editInput = e.target.previousElementSibling

        if (!editInput.classList.contains('edit-button__input--active')) {
            editInput.classList.add('edit-button__input--active')
        } else {
            const textForEditing = editInput.value
            const chousenTask = e.target.closest(`.${taskSelector}`)
            const chousenTaskId = +chousenTask.dataset.id

            onClickEditTaskButton(chousenTaskId, textForEditing)

            editInput.classList.remove('edit-button__input--active')

            cleanInputValue(addTaskInput)

            setTimeout(() => {
                cleanDomListBeforeRender(tasksParent)

                render(todoAppModel, tasksParent)
            }, 400)

        }
    })
}