import {
    taskSelector,
    taskTemplateSelector,
    taskTextSelector,
    taskTemplateContainer
} from './todoApp.dom.js'

const WHEN_NO_TASKS_TEXT = 'No tasks yet...'

const checkForNoTasks = (tasksList, tasksParent) => (!tasksList.length) && (tasksParent.textContent = WHEN_NO_TASKS_TEXT)

const render = (tasksList, whereToRender) => {
    const tasksElementsArray = tasksList.map(({ id, text }) => {
        const taskLi = document.createElement('li')

        taskLi.classList.add(taskSelector)
        taskLi.setAttribute('data-id', id);

        const template = document.getElementById(taskTemplateSelector)
        const taskText = template.content.querySelector(taskTextSelector)

        let taskContainer = document.getElementById(taskTemplateContainer)
        taskText.textContent = text

        taskContainer = template.content.cloneNode(true)
        taskLi.appendChild(taskContainer)

        return taskLi
    })

    tasksElementsArray.forEach(task => whereToRender.appendChild(task))
}

export { render, checkForNoTasks }