export const domState = {
    burgerSelector: '.burger',
    menuSelector: '.menu',
    activeClass: 'opened',
}