import { domState } from "./burger.dom.js"

const { burgerSelector, menuSelector, activeClass } = domState

export const useBurger = () => ({
    burgerSelector,
    menuSelector,
    activeClass
})