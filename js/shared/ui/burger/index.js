import { useBurger } from './burger.useBurger.js'

const burger = () => {
    const { burgerSelector, menuSelector, activeClass } = useBurger()

    const burger = document.querySelector(burgerSelector)
    const menu = document.querySelector(menuSelector)

    const toggleMenu = () => menu.classList.toggle(activeClass)

    burger.addEventListener('click', () => toggleMenu())
}

export { burger }
