import { domState } from './slider.dom.js'

const { activeSlideSelector } = domState

export const initVisibleSlide = (index = 0, slidesElements) => {
    slidesElements[index].classList.add(activeSlideSelector)
}