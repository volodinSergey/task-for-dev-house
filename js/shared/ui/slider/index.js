import { slidesElements } from './slider.render.js'
import { initVisibleSlide } from './slider.initVisibleSlide.js'
import { useButtons } from './slider.useButtons.js'

export const slider = () => {
    initVisibleSlide(0, slidesElements)

    const { prevButton, nextButton, moveUp, moveBack } = useButtons()

    prevButton.addEventListener('click', moveBack)
    nextButton.addEventListener('click', moveUp)
}