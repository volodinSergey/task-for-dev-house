import { domState } from './slider.dom.js'

const slidesModel = [
    '../../../../assets/images/slider/1.jpg',
    '../../../../assets/images/slider/2.webp',
    '../../../../assets/images/slider/3.jpg',
    '../../../../assets/images/slider/4.jpg',
    '../../../../assets/images/slider/5.jpg',
    '../../../../assets/images/slider/6.jpg',
]

const {
    slidesBoxSelector,
    slideSelector,
} = domState

const slidesBox = document.querySelector(slidesBoxSelector)

const render = (imagesArray, whereToRender) => {
    const slidesElementsArray = imagesArray.map(slide => {
        const newSlide = document.createElement('img')

        newSlide.classList.add(slideSelector)
        newSlide.src = slide
        newSlide.alt = 'slide'

        return newSlide
    })

    slidesElementsArray.forEach(slide => whereToRender.appendChild(slide))
}

render(slidesModel, slidesBox)

const slidesElements = [...slidesBox.children]

export { slidesElements }

