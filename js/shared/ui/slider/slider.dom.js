export const domState = {
    slidesBoxSelector: '.slider__slides',
    slideSelector: 'slider__slides-slide',
    prevButtonSelector: '.button-prev',
    nextButtonSelector: '.button-next',
    activeSlideSelector: 'active-slide',
}
