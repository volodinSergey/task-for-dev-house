import { domState } from './slider.dom.js'
import { initVisibleSlide } from './slider.initVisibleSlide.js'
import { slidesElements } from './slider.render.js'

const { activeSlideSelector, prevButtonSelector, nextButtonSelector } = domState

const prevButton = document.querySelector(prevButtonSelector)
const nextButton = document.querySelector(nextButtonSelector)

let index = 0

const moveUp = () => {
    index += 1

    if (index > slidesElements.length - 1) index = 0

    slidesElements.forEach(slide => slide.classList.remove(activeSlideSelector))

    initVisibleSlide(index, slidesElements)
}

const moveBack = () => {
    index -= 1

    if (index < 0) index = slidesElements.length - 1

    slidesElements.forEach(slide => slide.classList.remove(activeSlideSelector))

    initVisibleSlide(index, slidesElements)
}

export const useButtons = () => ({
    prevButton,
    nextButton,
    moveUp,
    moveBack,
})