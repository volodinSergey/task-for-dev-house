const cardAnimation = () => {
    const card = document.querySelector('.card')
    const cardItem = document.querySelector('.card__item')

    const startRotate = e => {
        const halfHeight = cardItem.offsetHeight / 2
        const halfWidth = cardItem.offsetWidth / 2

        cardItem.style.transform = `rotateX(${-(e.offsetY - halfHeight) / 6}deg) rotateY(${-(e.offsetX - halfWidth) / 6}deg)`
    }

    const stopRotate = () => cardItem.style.transform = 'rotate(0)'

    card.addEventListener('mousemove', startRotate)
    card.addEventListener('mouseout', stopRotate)
}

export { cardAnimation }

