export class TextEditorService {
    text = null

    take = incomingText => {
        this.text = incomingText

        return this
    }

    toLowerCase = () => {
        this.text = this.text.toLowerCase()

        return this
    }

    deleteGaps = () => {
        this.text = this.text.trim()

        return this
    }

    getEditedText = () => this.text
}



