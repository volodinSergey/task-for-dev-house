import { newTaskFactory } from '../../todoApp/newTask.factory.js'
import { TextEditorService } from '../../shared/helpServices/TextEditor.js'

let todoAppModel = [
    {
        id: Math.random(),
        text: 'Example task. Have fun and add more',
    },
]

class TodoService {
    addTask = newTaskText => {
        const textEditor = new TextEditorService()

        const normalizedNewTaskText = textEditor.take(newTaskText).deleteGaps().getEditedText()
        const newTask = newTaskFactory(normalizedNewTaskText)

        todoAppModel = [newTask, ...todoAppModel]
    }

    deleteTask = id => todoAppModel = todoAppModel.filter(task => +task.id !== id)

    editTask = (editedTaskId, textForEditing) => {
        const foundedTaskById = todoAppModel.find(({ id }) => id === editedTaskId)

        foundedTaskById.text = textForEditing || 'Hey, write something'
    }
}

export { todoAppModel, TodoService }