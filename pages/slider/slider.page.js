import { burger } from '../../js/shared/ui/burger/index.js'
import { slider } from '../../js/shared/ui/slider/index.js'

document.addEventListener('DOMContentLoaded', () => {
    burger()

    slider()
})