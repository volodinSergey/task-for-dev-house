import { burger } from '../../js/shared/ui/burger/index.js'

import { todoApp } from '../../js/todoApp/index.js'

document.addEventListener('DOMContentLoaded', () => {
    burger()

    todoApp()
})