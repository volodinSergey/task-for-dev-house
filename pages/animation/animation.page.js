import { cardAnimation } from '../../js/shared/animations/index.js'
import { burger } from '../../js/shared/ui/burger/index.js'

document.addEventListener('DOMContentLoaded', () => {
    burger()

    cardAnimation()
})